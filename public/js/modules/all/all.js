yOSON.AppCore.addModule("responsive-user-menu", function(){
    var st = {
        lnkMenu: ".show_menu",
        overlay: ".b-right-side",
        menusToShow: ".b-menu"
    };
    var DOM = {};
    var catchDOM = function(){
        DOM.lnkMenu = $(st.lnkMenu);
        DOM.menusToShow = $(st.menusToShow);
        DOM.overlay = $(st.overlay);
    };
    var subscribeEvents = function(){
        DOM.lnkMenu.on("click", events.showMenus);
        DOM.overlay.on("click", events.hideMenus);
    };
    var events = {
        showMenus: function(evt){
           DOM.menusToShow.fadeIn();
           evt.stopPropagation();
           evt.preventDefault();
        },
        hideMenus: function(){
           DOM.menusToShow.fadeOut();
        }
    };
    return {
        init: function(){
            $(function(){
                catchDOM();
                subscribeEvents();
            });
        }
    };
});
yOSON.AppCore.runModule("responsive-user-menu");
